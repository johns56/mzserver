import haversine
import logging
import json

from geopy.geocoders import Nominatim
from datetime import datetime, timedelta
from flask import Flask, request, jsonify
from pymongo import MongoClient, database, collection


app = Flask(__name__)
client = MongoClient()  # "127.0.0.1", 27017
server_db: database.Database = client.mz_database
locations: collection.Collection = server_db.locations

logging.basicConfig(level=logging.WARNING)
logging.info("Starting REST API server...")

if len(list(locations.list_indexes())) < 2:
    logging.error('No location index found. creating one...')
    locations.create_index([('location', '2dsphere')])
else:
    logging.info('Indexes status is OK')


@app.route('/locations', methods=['POST'])
def handle_locations():
    logging.debug(f"Address {request.remote_addr} body: {request.data}")
    data = request.get_json(force=True)  # header doesn't have to include json announcement
    request_type, longitude, latitude = data['type'], data['longitude'], data['latitude']
    result = {'status': False}

    # insert operation
    if data['type'] == 'report':
        description = data['description']
        address = 'unknown'

        try:
            # from geopy.geocoders import Nominatim

            coder = Nominatim(user_agent='Dubian user agent')  # Mozilla/5.0 (Windows NT 10.0; Win64; x64)
            geo_result = coder.reverse(f'{latitude}, {longitude}')
            address = geo_result.address

        except Exception as e:
            logging.error(f"Unable to resolve coordinstaes: {str(e)}")

        locations.insert_one({
            'location': {
                'type': 'Point',
                'coordinates': [longitude, latitude]
            },
            'description': description,
            'address': address,
            'time_inserted': datetime.now()
        })

        result['status'] = True

        logging.info('Inserted new data')

    # query operation
    elif data['type'] == 'scan':
        distance, duration, sensitivity = data['distance'], data['duration'], data['sensitivity']
        limit_time = datetime.now() - timedelta(hours=duration)

        query = {
            'location': {
                '$near': {
                    '$geometry': {
                        'type': 'Point',
                        'coordinates': [longitude, latitude]
                    },
                    '$maxDistance':
                        distance * 1000  # in meters
                }
            },
            'time_inserted': {
                '$gte': limit_time
            }
        }

        # check the number of reports returnes is bigger then the client sensitivity
        results_len = len(list(locations.find(query)))
        if results_len >= sensitivity:
            query_result = locations.find_one(query)
            # calculate the distance to the closest point
            coordinates = query_result['location']['coordinates']
            distance_to_point = haversine.haversine((coordinates[1], coordinates[0]), (latitude,
                                                                                       longitude))
            # set the results
            result['distance'] = round(distance_to_point, 2)
            result['found'] = True

        # The area is clean
        else:
            result['found'] = False

        # overall we had successful operation, don't you think?
        result['status'] = True
        logging.info('queried some data')

    return jsonify(result)


@app.route('/reports', methods=['POST', 'GET'])
def handle_reports():
    logging.debug(f"Address {request.remote_addr} body: {request.data}")
    limit_time = datetime.now() - timedelta(days=1)  # get all reports one day back

    query_result = locations.find({
        'time_inserted': {
            '$gte': limit_time
        }
    }, {'_id': 0, 'address': 1, 'description': 1})

    query_result = list(query_result)
    query_result.reverse()

    return json.dumps(query_result)


app.run(host='0.0.0.0', port=8080)
